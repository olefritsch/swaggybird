//
//  GameScene.swift
//  MyGameProject
//
//  Created by Ole Fritsch on 08/04/16.
//  Copyright (c) 2016 Ole Fritsch. All rights reserved.
//

import SpriteKit
import AVFoundation

struct PhysicsCategories {
    static let Bird : UInt32 = 0x1 << 1
    static let Ground : UInt32 = 0x1 << 2
    static let Pipe : UInt32 = 0x1 << 3
    static let Score : UInt32 = 0x1 << 4
}

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    var Ground = SKSpriteNode()
    var Bird = SKSpriteNode()
    var pipes = SKNode()
    
    var moveAndRemove = SKAction()
    
    var gameStarted : Bool = false
    var gameOver : Bool = false
    var restartBtn = SKLabelNode()
    var score : Int = 0
    var scoreLabel = SKLabelNode()
    var gameOverLabel = SKLabelNode()
    
    var wingAudioURL = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("sfx_wing", ofType: "mp3")!)
    var pointAudioURL = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("sfx_point", ofType: "mp3")!)
    var dieAudioURL = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("sfx_die", ofType: "mp3")!)
    var hitAudioURL = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("sfx_hit", ofType: "mp3")!)
    
    var wingAudio = AVAudioPlayer()
    var pointAudio = AVAudioPlayer()
    var dieAudio = AVAudioPlayer()
    var hitAudio = AVAudioPlayer()
    
    var scoreNode = SKSpriteNode()
    
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
        
        startGame()
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        let bodyA = contact.bodyA
        let bodyB = contact.bodyB
        
        if ((bodyA.categoryBitMask == PhysicsCategories.Bird && bodyB.categoryBitMask == PhysicsCategories.Score) ||
            (bodyA.categoryBitMask == PhysicsCategories.Score && bodyB.categoryBitMask == PhysicsCategories.Bird)) {
            
            pointAudio.play()
            score += 1
            scoreLabel.text = "\(score)"
        }
        
        if ((bodyA.categoryBitMask == PhysicsCategories.Bird && bodyB.categoryBitMask == PhysicsCategories.Pipe) ||
            (bodyA.categoryBitMask == PhysicsCategories.Pipe && bodyB.categoryBitMask == PhysicsCategories.Bird) ||
            (bodyA.categoryBitMask == PhysicsCategories.Bird && bodyB.categoryBitMask == PhysicsCategories.Ground) ||
            (bodyA.categoryBitMask == PhysicsCategories.Ground && bodyB.categoryBitMask == PhysicsCategories.Bird)) {
            
            if !gameOver {
                hitAudio.play()
                dieAudio.play()
                endGame()

            }
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
       /* Called when a touch begins */
        
        if gameStarted {
            if gameOver {
                
            } else {
                wingAudio.play()
                Bird.physicsBody?.velocity = CGVectorMake(0, 0)
                Bird.physicsBody?.applyImpulse(CGVectorMake(0, 60))
            }
            
        } else {
            wingAudio.play()
            gameStarted = true
            
            let spawn = SKAction.runBlock {
                
                self.spawnPipes()
            }
            let delay = SKAction.waitForDuration(2.5)
            let spawnSequence = SKAction.sequence([spawn, delay])
            let spawnLoop = SKAction.repeatActionForever(spawnSequence)
            self.runAction(spawnLoop)
            
            let distance = CGFloat(self.frame.width + pipes.frame.width)
            let movement = SKAction.moveByX(-distance, y: 0, duration: NSTimeInterval(0.01 * distance))
            moveAndRemove = SKAction.sequence([movement, SKAction.removeFromParent()])
            
            Bird.physicsBody?.affectedByGravity = true
            
            Bird.physicsBody?.velocity = CGVectorMake(0, 0)
            Bird.physicsBody?.applyImpulse(CGVectorMake(0, 60))
        }
        
        for touch in touches {
            let location = touch.locationInNode(self)
            
            if gameOver {
                if restartBtn.containsPoint(location) {
                    restartGame()
                }
            }
        }
        
    }
    
    func endGame() {
        gameOver = true
        gameOverLabel.text = "Game Over"
        gameOverLabel.fontName = "04b_19"
        gameOverLabel.fontSize = self.frame.width / 15
        gameOverLabel.fontColor = SKColor.orangeColor()
        gameOverLabel.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 1.5)
        gameOverLabel.zPosition = 60
        
        gameOverLabel.removeFromParent()
        self.addChild(gameOverLabel)
        
        enumerateChildNodesWithName("pipes", usingBlock: {(node, error) in
            
            node.speed = 0
            self.removeAllActions()
        })
        
        createHighscoreNode()
        createRestartBtn()
    }
    
    func startGame() {
        self.physicsWorld.contactDelegate = self
        
        for i in 0..<2 {
            let background = SKSpriteNode(imageNamed: "Background")
            background.anchorPoint = CGPointZero
            background.position = CGPointMake(CGFloat(i) * background.frame.width, 0)
            background.name = "background"
            self.addChild(background)
        }
        
        Ground = SKSpriteNode(imageNamed: "Ground")
        Ground.setScale(0.5)
        Ground.position = CGPoint(x: self.frame.width / 2, y: 0 + Ground.frame.height / 2)
        Ground.zPosition = 30;
        
        Ground.physicsBody = SKPhysicsBody(rectangleOfSize: Ground.size)
        Ground.physicsBody?.categoryBitMask = PhysicsCategories.Ground
        Ground.physicsBody?.collisionBitMask = PhysicsCategories.Bird
        Ground.physicsBody?.contactTestBitMask = PhysicsCategories.Bird
        Ground.physicsBody?.affectedByGravity = false
        Ground.physicsBody?.dynamic = false
        
        self.addChild(Ground)
        
        Bird = SKSpriteNode(imageNamed: "SwaggyBird")
        Bird.size = CGSize(width: 60, height: 70)
        Bird.position = CGPoint(x: self.frame.width / 2 - Bird.frame.width, y: self.frame.height / 2 + Bird.frame.height)
        Bird.zPosition = 20;
        
        Bird.physicsBody = SKPhysicsBody(circleOfRadius: Bird.frame.width / 2)
        Bird.physicsBody?.categoryBitMask = PhysicsCategories.Bird
        Bird.physicsBody?.collisionBitMask = PhysicsCategories.Ground | PhysicsCategories.Pipe
        Bird.physicsBody?.contactTestBitMask = PhysicsCategories.Ground | PhysicsCategories.Pipe
        Bird.physicsBody?.affectedByGravity = false
        Bird.physicsBody?.dynamic = true
        
        self.addChild(Bird)
        
        scoreLabel.position = CGPoint(x: self.frame.width / 2, y: self.frame.height - self.frame.height / 6)
        scoreLabel.zPosition = 15;
        scoreLabel.fontName = "04b_19"
        scoreLabel.fontSize = self.frame.height / 12
        scoreLabel.fontColor = SKColor.yellowColor()
        scoreLabel.text = "\(score)"
        self.addChild(scoreLabel)
        
        wingAudio = try!AVAudioPlayer(contentsOfURL: wingAudioURL, fileTypeHint: nil)
        pointAudio = try!AVAudioPlayer(contentsOfURL: pointAudioURL, fileTypeHint: nil)
        dieAudio = try!AVAudioPlayer(contentsOfURL: dieAudioURL, fileTypeHint: nil)
        hitAudio = try!AVAudioPlayer(contentsOfURL: hitAudioURL, fileTypeHint: nil)
    }
    
    func restartGame() {
        self.removeAllActions()
        self.removeAllChildren()
        gameOver = false
        gameStarted = false
        score = 0;
        startGame()
    }
    
    func createRestartBtn() {
        restartBtn = SKLabelNode(text: "Restart")
        restartBtn.color = SKColor.lightGrayColor()
        restartBtn.fontColor = SKColor.orangeColor()
        restartBtn.fontName = "04b_19"
        restartBtn.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2 - self.frame.height / 3)
        restartBtn.zPosition = 50
        self.addChild(restartBtn)
    }
    
    func createHighscoreNode() {
        let highscore = getHighscore()
        
        if highscore == score {
            scoreLabel.text = "New Highscore!"
            scoreLabel.fontSize = self.frame.height / 16
        }
        
        let currentScoreLabel = SKLabelNode(text: "Score: \(score)")
        currentScoreLabel.fontName = "04b_19"
        let highscoreLabel = SKLabelNode(text: "Highscore: \(highscore)")
        highscoreLabel.fontName = "04b_19"
        
        scoreNode = SKSpriteNode(color: SKColor.lightGrayColor(), size: CGSize(width: self.frame.width / 3.5, height: self.frame.height / 5))
        scoreNode.addChild(currentScoreLabel)
        scoreNode.addChild(highscoreLabel)
        currentScoreLabel.position.y = currentScoreLabel.position.y + currentScoreLabel.frame.height / 1.5
        highscoreLabel.position.y = highscoreLabel.position.y - highscoreLabel.frame.height / 1.5
        scoreNode.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
        scoreNode.zPosition = 80
        self.addChild(scoreNode)
    }
    
    func spawnPipes() {
        
        let scoreNode = SKSpriteNode()
        scoreNode.size = CGSize(width: 1, height: self.frame.height)
        scoreNode.position = CGPoint(x: self.frame.width, y: self.frame.height / 2)
        scoreNode.physicsBody = SKPhysicsBody(rectangleOfSize: scoreNode.size)
        scoreNode.physicsBody?.categoryBitMask = PhysicsCategories.Score
        scoreNode.physicsBody?.collisionBitMask = 0
        scoreNode.physicsBody?.contactTestBitMask = PhysicsCategories.Bird
        scoreNode.physicsBody?.affectedByGravity = false
        scoreNode.physicsBody?.dynamic = false
        
        pipes = SKNode()
        pipes.name = "pipes"
        
        let topPipe = SKSpriteNode(imageNamed: "Pipe")
        let btmPipe = SKSpriteNode(imageNamed: "Pipe")
        
        topPipe.position = CGPoint(x: self.frame.width, y: self.frame.height / 2 + 300)
        btmPipe.position = CGPoint(x: self.frame.width, y: self.frame.height / 2 - 300)
        
        topPipe.setScale(0.5)
        btmPipe.setScale(0.5)
        
        topPipe.physicsBody = SKPhysicsBody(rectangleOfSize: topPipe.size)
        topPipe.physicsBody?.categoryBitMask = PhysicsCategories.Pipe
        topPipe.physicsBody?.collisionBitMask = PhysicsCategories.Bird
        topPipe.physicsBody?.contactTestBitMask = PhysicsCategories.Bird
        topPipe.physicsBody?.affectedByGravity = false
        topPipe.physicsBody?.dynamic = false
        
        btmPipe.physicsBody = SKPhysicsBody(rectangleOfSize: btmPipe.size)
        btmPipe.physicsBody?.categoryBitMask = PhysicsCategories.Pipe
        btmPipe.physicsBody?.collisionBitMask = PhysicsCategories.Bird
        btmPipe.physicsBody?.contactTestBitMask = PhysicsCategories.Bird
        btmPipe.physicsBody?.affectedByGravity = false
        btmPipe.physicsBody?.dynamic = false
        
        topPipe.zRotation = CGFloat(M_PI)
        
        pipes.addChild(topPipe)
        pipes.addChild(btmPipe)
        pipes.addChild(scoreNode)
        pipes.zPosition = 10;
        pipes.position.y = pipes.position.y + self.randomCGFloat(-100, 100)
        
        pipes.runAction(moveAndRemove)
        self.addChild(pipes)
        
    }
    
    func getHighscore() -> Int {
        let defaults = NSUserDefaults.standardUserDefaults()
        let highscore = defaults.integerForKey("Highscore")
        
        if score > highscore {
            defaults.setInteger(score, forKey: "Highscore")
            return score
        } else {
            return highscore + 1
        }
    }
    
    func randomCGFloat(min : CGFloat, _ max : CGFloat) -> CGFloat {
        let random = CGFloat(Float(arc4random()) / 0xFFFFFFFF)
        return random * (max - min) + min
    }
   
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
        
        if gameStarted {
            if !gameOver {
                enumerateChildNodesWithName("background", usingBlock: ({
                    (node, error) in
                    
                    let background = node as! SKSpriteNode
                    background.position = CGPoint(x: background.position.x - 0.5, y: background.position.y)
                    
                    if background.position.x <= -background.frame.width {
                        background.position = CGPointMake(background.frame.width, background.position.y)
                    }
                }))
            }
        }
    }
}
